1.First of all, unzip the Calculator.zip file.
2.After you have to put the follow files in the same folder:
    calculator-api-1.0.jar
    calculator-app-1.0.jar
    calculator-operation-impl-1.0.jar
    input.txt

Follow the steps to run the application:

1.Open a command line prompt from the jar file folder.
2.Run this command: java -jar calculator-app-1.0.jar input.txt output.txt.
3.The error message appear in command line prompt and the result is in the output.txt

Follow the steps to extend the application:

1.Write the new operation in input.txt file.
2.Open the calculator-app-1.0.jar and modify the config.properties file. Complete the operation_keyword and operation_name in the following row and
add the row in the end of config.properties:
    operation_keyword=calculator.operations.operation_name
3.Copy the extension jar in the folder, with calculator-extension.jar name.
4.Run this command:
    java -cp calculator-api.jar;calculator-app.jar;calculator-operation-impl.jar;calculator-extension.jar calculator.app.Main input.txt output.txt