package calculator.engine;

import calculator.operations.*;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.StringJoiner;
import static org.junit.Assert.*;

public class CorrectnessCheckerTest {

    private CorrectnessChecker correctnessChecker;
    private List<OperationsComponents> handlers;

    @Before
    public void setUp() {
        correctnessChecker = new CorrectnessChecker();
        handlers = Arrays.asList(
                new Adder(),
                new Subtractor(),
                new Multiplier(),
                new Divider(),
                new Power(),
                new Sum(),
                new Inverter(),
                new Average()
        );
    }

    private String setOperationRow(String[] elements) {
        StringJoiner stringJoiner = new StringJoiner(" ");

        for (String element : elements) {
            stringJoiner.add(element);
        }

        return stringJoiner.toString();
    }

    @Test(expected = InvalidStatementException.class)
    public void testEmptyRow() throws InvalidStatementException {
        String[] elements = new String[]{};
        String operationRow = setOperationRow(elements);
        correctnessChecker.check(operationRow, elements, "", handlers);
    }

    @Test(expected = InvalidStatementException.class)
    public void testRowWithGoodOperationWithoutElements() throws InvalidStatementException {
        String[] elements = new String[]{"add"};
        String keyword = elements[0];
        String operationRow = setOperationRow(elements);
        correctnessChecker.check(operationRow, elements, keyword, handlers);
    }

    @Test(expected = InvalidStatementException.class)
    public void testRowWithGoodSymbolForBinaryOperationWithoutElements() throws InvalidStatementException {
        String[] elements = new String[]{"+"};
        String keyword = elements[0];
        String operationRow = setOperationRow(elements);
        correctnessChecker.check(operationRow, elements, keyword, handlers);
    }

    @Test(expected = InvalidStatementException.class)
    public void testRowWithBadOperationWithoutElements() throws InvalidStatementException {
        String[] elements = new String[]{"haha"};
        String keyword = elements[0];
        String operationRow = setOperationRow(elements);
        correctnessChecker.check(operationRow, elements, keyword, handlers);
    }

    @Test(expected = InvalidStatementException.class)
    public void testRowWithUnaryOperationWithMoreElements() throws InvalidStatementException {
        String[] elements = new String[]{"inv", "5.0", "9.0"};
        String keyword = elements[0];
        String operationRow = setOperationRow(elements);
        correctnessChecker.check(operationRow, elements, keyword, handlers);
    }

    @Test(expected = InvalidStatementException.class)
    public void testRowWithBinaryOperationWithOneElements() throws InvalidStatementException {
        String[] elements = new String[]{"add", "3.0"};
        String keyword = elements[0];
        String operationRow = setOperationRow(elements);
        correctnessChecker.check(operationRow, elements, keyword, handlers);
    }

    @Test(expected = InvalidStatementException.class)
    public void testRowWithBinaryOperationWithTreeElements() throws InvalidStatementException {
        String[] elements = new String[]{"add", "3.0", "2.0", "10.0"};
        String keyword = elements[0];
        String operationRow = setOperationRow(elements);
        correctnessChecker.check(operationRow, elements, keyword, handlers);
    }

    @Test
    public void testRowWithSymbolForBinaryOperationWithTwoElements() throws InvalidStatementException {
        String[] elements = new String[]{"+", "3.0", "5.0"};
        String keyword = elements[0];
        String operationRow = setOperationRow(elements);
        OperationsComponents handler = correctnessChecker.check(operationRow, elements, keyword, handlers);

        assertEquals(handler.getKeyword(), "add");
    }
}