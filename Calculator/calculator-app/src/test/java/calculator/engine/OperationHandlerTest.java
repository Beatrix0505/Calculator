package calculator.engine;

import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.StringJoiner;

import static org.junit.Assert.*;

public class OperationHandlerTest {
    private OperationHandler handler;
    private FileHandler fileHandler = new FileHandler();

    @Before
    public void setUp() throws NoSuchMethodException, IOException, InstantiationException, IllegalAccessException, InvocationTargetException, ClassNotFoundException {
        handler = new OperationHandler(fileHandler.configFileRead());
    }

    private String setOperationRow(String[] elements) {
        StringJoiner stringJoiner = new StringJoiner(" ");

        for (String element : elements) {
            stringJoiner.add(element);
        }

        return stringJoiner.toString();
    }

    @Test
    public void testGoodUnaryOperation() throws InvalidStatementException {
        String[] elements = new String[]{"inv", "2.0"};
        String operationRow = setOperationRow(elements);

        String resultRow = handler.parseOperationRow(operationRow);
        String expectedResultRow = "inv(2.0) = 0.5";

        assertEquals(resultRow, expectedResultRow);
    }

    @Test
    public void testGoodBinaryOperation() throws InvalidStatementException {
        String[] elements = new String[]{"add", "2.0", "10.0"};
        String operationRow = setOperationRow(elements);

        String resultRow = handler.parseOperationRow(operationRow);
        String expectedResultRow = "2.0 + 10.0 = 12.0";

        assertEquals(resultRow, expectedResultRow);
    }

    @Test
    public void testGoodAggregationOperation() throws InvalidStatementException {
        String[] elements = new String[]{"avg", "2.0", "9.0", "12.0", "6.0", "1.0"};
        String operationRow = setOperationRow(elements);

        String resultRow = handler.parseOperationRow(operationRow);
        String expectedResultRow = "avg(2.0, 9.0, 12.0, 6.0, 1.0) = 6.0";

        assertEquals(resultRow, expectedResultRow);
    }
}