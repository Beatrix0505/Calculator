package calculator;

import calculator.app.Main;
import org.junit.Assert;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class CalculatorIntegrationTest {

    private static final String OUTPUT_TXT = "output.txt";
    private static final String INPUT_TXT = "input.txt";
    private static final String EXPECTED_OUTPUT_TXT = "outputExpected.txt";

    @Test
    public void checkTheResultAreEqualToExpected() throws IOException, InvocationTargetException, NoSuchMethodException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        Main.main(new String[]{INPUT_TXT, OUTPUT_TXT});

        Assert.assertTrue(Files.exists(Paths.get(OUTPUT_TXT)));

        BufferedReader readOutput = Files.newBufferedReader(Paths.get(OUTPUT_TXT));
        BufferedReader readOutputExpected = Files.newBufferedReader(Paths.get(EXPECTED_OUTPUT_TXT));
        String lineOutput;
        String lineOutputExpected;

        lineOutput = readOutput.readLine();
        lineOutputExpected = readOutputExpected.readLine();

        while (lineOutput != null & lineOutputExpected != null){
            Assert.assertEquals(lineOutput, lineOutputExpected);

            lineOutput = readOutput.readLine();
            lineOutputExpected = readOutputExpected.readLine();
        }
    }
}
