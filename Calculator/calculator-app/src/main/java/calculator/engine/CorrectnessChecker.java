package calculator.engine;

import java.util.List;

class CorrectnessChecker {

    /**
     * Check the correctness of statement.
     * @param operationRow is the statement.
     * @param elements are the statement elements.
     * @param keyword is the operation keyword.
     * @param handlers are the possible operations.
     * @return the handler based on keyword.
     * @throws InvalidStatementException if something is wrong with the given statement.
     */
    OperationsComponents check(String operationRow, String[] elements, String keyword, List<OperationsComponents> handlers) throws InvalidStatementException {
        for (OperationsComponents handler : handlers) {
            if (elements.length == 0){
                throw new InvalidStatementException("It is an empty row", operationRow);
            }
            if (keyword.equalsIgnoreCase(handler.getKeyword()) || keyword.charAt(0) == handler.getSymbol()) {
                if (elements.length == 1) {
                    throw new InvalidStatementException("Wrong number of parts for binary/aggregation operation", operationRow);
                }
                if (handler instanceof UnaryOperationBase) {
                    if (elements.length != 2) {
                        throw new InvalidStatementException("Wrong number of parts for unary operation", operationRow);
                    }
                }
                if (handler instanceof BinaryOperationBase) {
                    if ((elements.length != 3)) {
                        throw new InvalidStatementException("Wrong number of parts for binary operation", operationRow);
                    }
                }
                return handler;
            }
        }
        throw new InvalidStatementException("Wrong operation keyword", operationRow);
    }
}
