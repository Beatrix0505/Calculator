package calculator.engine;

import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * This class managed the file read and write operations.
 */
public class FileHandler {
    private BufferedReader reader;
    private BufferedWriter writer;


    /**
     * This method is responsible for create the file reader and writer.
     *
     * @param inputName  the input file.
     * @param outputName the output file.
     */
    public FileHandler(String inputName, String outputName) {
        try {
            reader = Files.newBufferedReader(Paths.get(inputName));
            writer = Files.newBufferedWriter(Paths.get(outputName));
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    public FileHandler() {
    }

    /**
     * This method reads one row from the file.
     *
     * @return the row in string format.
     */
    public String readFile() {
        try {
            return reader.readLine();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    /**
     * This method writes one row in the file.
     *
     * @param result it`s the string with the equation result.
     */
    public void writeFile(String result) {
        try {
            writer.write(result);
            writer.newLine();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * This method is responsible for close a file reader and writer.
     */
    public void close() {
        try {
            reader.close();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * This method reads the config.properties file and with reflection creates an instance for all operations.
     * @return the list with all operations instance.
     * @throws IOException if something is wrong.
     * @throws ClassNotFoundException if something is wrong.
     * @throws NoSuchMethodException if something is wrong.
     * @throws InvocationTargetException if something is wrong.
     * @throws InstantiationException if something is wrong.
     * @throws IllegalAccessException if something is wrong.
     */
    public List<OperationsComponents> configFileRead() throws IOException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        ClassLoader classLoader = getClass().getClassLoader();
        InputStream inputStream = classLoader.getResourceAsStream("config.properties");
        try (BufferedReader configReader = new BufferedReader(new InputStreamReader(inputStream))) {

            String configRow;

            final List<OperationsComponents> listOperationsComponents = new ArrayList<>();
            OperationsComponents operationsComponents;

            while ((configRow = configReader.readLine()) != null) {
                String[] configElements = configRow.split("=");
                operationsComponents = createOperation(configElements[1]);
                listOperationsComponents.add(operationsComponents);
            }

            return listOperationsComponents;
        }

    }

    /**
     * This method realises the reflection.
     * @param operationName the name of operation class.
     * @return the operation class instance.
     * @throws ClassNotFoundException if something is wrong.
     * @throws NoSuchMethodException if something is wrong.
     * @throws IllegalAccessException if something is wrong.
     * @throws InvocationTargetException if something is wrong.
     * @throws InstantiationException if something is wrong.
     */
    private OperationsComponents createOperation(String operationName) throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        Class<?> operationClass = Class.forName(operationName);
        Constructor<?> operationClassConstructor = operationClass.getConstructor();
        return (OperationsComponents) operationClassConstructor.newInstance();
    }
}
