package calculator.engine;

import java.util.List;
import java.util.StringJoiner;

/**
 * This class solves the equations.
 */

public class OperationHandler {
    private char EQUAL_SYMBOL = '=';
    private String SEPARATOR = " ";

    private List<OperationsComponents> handlers;

    public OperationHandler(List<OperationsComponents> handlers) {
        this.handlers = handlers;
    }


    /**
     * This method checks the correctness of equation, splits and solves it.
     *
     * @param operationRow is the equation in string format.
     * @return a result in string format.
     * @throws InvalidStatementException if something went wrong.
     */
    public String parseOperationRow(String operationRow) throws InvalidStatementException {
        String[] elements = operationRow.split(SEPARATOR);
        String keyword = elements[0];

        CorrectnessChecker checker = new CorrectnessChecker();
        OperationsComponents handler = checker.check(operationRow, elements, keyword, handlers);

        if (handler instanceof UnaryOperationBase){
            return solveUnaryOperations(elements, handler);
        }
        else {
            if (handler instanceof BinaryOperationBase) {
                return solveBinaryOperations(elements, handler);
            } else {
                return solveAggregationOperations(elements, handler);
            }
        }
    }

    /**
     * This method solve the UnaryEquation.
     *
     * @param elements     is the parsed equation.
     * @param handler      is the founded UnaryEquation.
     * @return the result in string format.
     */
    private String solveUnaryOperations(String[] elements, OperationsComponents handler) throws InvalidStatementException {
        double value = Double.parseDouble(elements[1]);

        double result = handler.calculation(value);
        return resultFormatForUnaryOperations(value, result, handler);
    }

    /**
     * This method solve the BinaryEquation.
     *
     * @param elements     is the parsed equation.
     * @param handler      is the founded BinaryEquation.
     * @return the result in string format.
     */
    private String solveBinaryOperations(String[] elements, OperationsComponents handler) throws InvalidStatementException {
        double lValue = Double.parseDouble(elements[1]);
        double rValue = Double.parseDouble(elements[2]);

        double result = handler.calculation(lValue, rValue);
        return resultFormatForBinaryOperations(lValue, rValue, result, handler);
    }

    /**
     * This method solve the AggregationEquation.
     *
     * @param elements           is the parsed equation.
     * @param thisComplexHandler is the founded AggregationEquation.
     * @return the result in string format.
     */

    private String solveAggregationOperations(String[] elements, OperationsComponents thisComplexHandler) throws InvalidStatementException {
        int size = elements.length;
        double[] value = new double[size - 1];
        for (int i = 0; i < size - 1; i++) {
            value[i] = Double.parseDouble(elements[i + 1]);
        }
        double result = thisComplexHandler.calculation(value);
        return resultFormatForAggregationOperations(value, result, thisComplexHandler);
    }

    /**
     * This method creates the result string in UnaryEquation case.
     *
     * @param value  is the value from equation.
     * @param result  is the result of equation.
     * @param handler is the operation type.
     * @return a string with equation and result.
     */
    private String resultFormatForUnaryOperations(double value, double result, OperationsComponents handler) {
        StringJoiner resultEquation = new StringJoiner("");
        resultEquation.add(handler.getKeyword())
                      .add(Character.toString('('))
                      .add(Double.toString(value))
                      .add(Character.toString(')'))
                      .add(SEPARATOR)
                      .add(Character.toString(EQUAL_SYMBOL))
                      .add(SEPARATOR)
                      .add(Double.toString(result));

        return resultEquation.toString();
    }

    /**
     * This method creates the result string in BinaryEquation case.
     *
     * @param lValue  is the left value from equation.
     * @param rValue  is the right value from equation.
     * @param result  is the result of equation.
     * @param handler is the operation type.
     * @return a string with equation and result.
     */
    private String resultFormatForBinaryOperations(double lValue, double rValue, double result, OperationsComponents handler) {
        StringJoiner resultEquation = new StringJoiner(SEPARATOR);
        resultEquation
                .add(Double.toString(lValue))
                .add(Character.toString(handler.getSymbol()))
                .add(Double.toString(rValue))
                .add(Character.toString(EQUAL_SYMBOL))
                .add(Double.toString(result));

        return resultEquation.toString();
    }

    /**
     * This method creates the result string in AggregationEquation case.
     *
     * @param value   is the values from equation.
     * @param result  is the result of equation.
     * @param handler is the operation type.
     * @return a string with equation and result.
     */

    private String resultFormatForAggregationOperations(double[] value, double result, OperationsComponents handler) {
        StringJoiner resultEquation = new StringJoiner("");
        resultEquation.add(handler.getKeyword());
        resultEquation.add(Character.toString('('));
        for (int i = 0; i < value.length; i++) {
            resultEquation.add(Double.toString(value[i]));
            if (i + 1 < value.length) {
                resultEquation.add(", ");
            }
        }
        resultEquation.add(") ")
                .add(Character.toString(EQUAL_SYMBOL))
                .add(" ")
                .add(Double.toString(Double.parseDouble(String.format("%.1f", result))));

        return resultEquation.toString();

    }
}
