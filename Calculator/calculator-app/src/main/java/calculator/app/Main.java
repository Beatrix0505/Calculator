package calculator.app;

import calculator.engine.FileHandler;
import calculator.engine.OperationHandler;
import calculator.engine.InvalidStatementException;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Main {

    private static final int NR_ARGS =2;

    public static void main(String[] args) throws NoSuchMethodException, IOException, InstantiationException, IllegalAccessException, InvocationTargetException, ClassNotFoundException {

        checkCorrectnessOfArgs(args);

        FileHandler fileHandler = new FileHandler(args[0], args[1]);

        OperationHandler handler = new OperationHandler(fileHandler.configFileRead());

        String equation;
        String result;
        while ((equation = fileHandler.readFile()) != null){
            try {
                result = handler.parseOperationRow(equation);
                fileHandler.writeFile(result);
            } catch (InvalidStatementException e) {
                System.out.println(e.getMessage());
            }
        }
        fileHandler.close();
    }

    private static void checkCorrectnessOfArgs(String[] args){
        try{
            correctnessOfArgs(args);
        }
        catch(IllegalArgumentException e){
            System.out.println(e.getMessage());
        }
    }

    private static void correctnessOfArgs(String args[]){
        if (!Files.exists(Paths.get(args[0]))){
            throw new IllegalArgumentException("The input file doesn`t exists!");
        }
        if (!Files.isReadable(Paths.get(args[0]))){
            throw new IllegalArgumentException("The input file doesn`t readable");
        }
        if (args.length != NR_ARGS){
            throw new IllegalArgumentException("The arguments number must be" + NR_ARGS);
        }
    }
}
