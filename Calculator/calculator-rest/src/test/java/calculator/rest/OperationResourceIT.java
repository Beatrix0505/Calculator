package calculator.rest;

import calculator.rest.transferobject.OperationResultTransferObject;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.io.InputStream;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class OperationResourceIT {

    private static final String HTTP_LOCALHOST_8080_CALCULATOR_REST_SINGLE_OPERATION = "http://localhost:8080/calculator-rest/singleOperation";
    private static final String HTTP_LOCALHOST_8080_CALCULATOR_REST_BATCH_OPERATIONS = "http://localhost:8080/calculator-rest/batchOperations";
    private Client client;

    @Before
    public void setUp() {
        client = ClientBuilder.newClient();
    }

    @Test
    public void testRESTForSingleOperation() throws IOException {
        // Given
        // Input JSON data
        byte[] inputJsonData = readJsonDataFromResource("single_input.json");
        // Expected output JSON data
        byte[] expectedJsonData = readJsonDataFromResource("expected_single_output.json");

        OperationResultTransferObject expectedResult =
                new ObjectMapper().readValue(expectedJsonData, OperationResultTransferObject.class);

        // When
        OperationResultTransferObject resultTransferObject = client
                .target(HTTP_LOCALHOST_8080_CALCULATOR_REST_SINGLE_OPERATION)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.entity(inputJsonData, MediaType.APPLICATION_JSON_TYPE), OperationResultTransferObject.class);

        // Then
        assertThat(expectedResult, is(resultTransferObject));
    }

    @Test
    public void testRESTForBatchOperations() throws IOException {
        // Given
        // Input JSON data
        byte[] inputJsonData = readJsonDataFromResource("input.json");
        // Expected output JSON data
        byte[] expectedJsonData = readJsonDataFromResource("expected_output.json");

        OperationResultTransferObject[] expectedResults =
                new ObjectMapper().readValue(expectedJsonData, OperationResultTransferObject[].class);

        // When
        OperationResultTransferObject[] resultTransferObjects = client
                .target(HTTP_LOCALHOST_8080_CALCULATOR_REST_BATCH_OPERATIONS)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .post(Entity.entity(inputJsonData, MediaType.APPLICATION_JSON_TYPE), OperationResultTransferObject[].class);

        // Then
        for (int i = 0; i < expectedResults.length; i++) {
            assertThat(expectedResults[i], is(resultTransferObjects[i]));
        }
    }

    private byte[] readJsonDataFromResource(String resourceName) throws IOException {
        final ClassLoader classLoader = this.getClass().getClassLoader();
        final InputStream inputStream = classLoader.getResourceAsStream(resourceName);

        return IOUtils.toByteArray(inputStream);
    }
}
