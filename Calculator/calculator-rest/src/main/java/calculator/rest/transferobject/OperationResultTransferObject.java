package calculator.rest.transferobject;

import java.util.List;
import java.util.Objects;

public class OperationResultTransferObject {
    private String operation;
    private List<Double> operands;
    private Double result;

    public OperationResultTransferObject() {
    }

    public OperationResultTransferObject(String operation, List<Double> operands, Double result) {
        this.operation = operation;
        this.operands = operands;
        this.result = result;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public List<Double> getOperands() {
        return operands;
    }

    public void setOperands(List<Double> operands) {
        this.operands = operands;
    }

    public Double getResult() {
        return result;
    }

    public void setResult(Double result) {
        this.result = result;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        if (this == obj) {
            return true;
        }

        if (!(obj instanceof OperationResultTransferObject)) {
            return false;
        } else {
            OperationResultTransferObject transferObject =
                    (OperationResultTransferObject) obj;

            return Objects.equals(getOperation(), transferObject.getOperation()) &&
                    Objects.equals(getOperands(), transferObject.getOperands()) &&
                    Objects.equals(getResult(), transferObject.getResult());
        }
    }

    @Override
    public String toString() {
        return "OperationResultTransferObject{" +
                "operation='" + operation + '\'' +
                ", operands=" + operands +
                ", result=" + result +
                '}';
    }
}
