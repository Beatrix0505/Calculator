package calculator.rest.transferobject;

import java.util.List;
import java.util.StringJoiner;

public class OperationTransferObject<OPERANDS> {
    private String operation;
    private OPERANDS operands;

    public OperationTransferObject() {
    }

    public String getOperation() {
        return operation;
    }

    public OPERANDS getOperands() {
        return operands;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public void setOperands(OPERANDS operands) {
        this.operands = operands;
    }

    public String createInputForOperationHandler(){
        StringJoiner input = new StringJoiner(" ");
        input.add(operation);

        if (operands != null) {
            if (operands instanceof List){
                for (Object o: (List) operands) {
                    input.add(o.toString());
                }
            } else {
                input.add(operands.toString());
            }
        }

        return input.toString();
    }
}
