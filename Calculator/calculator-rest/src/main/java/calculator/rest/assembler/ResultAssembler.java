package calculator.rest.assembler;

import calculator.rest.transferobject.OperationResultTransferObject;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ResultAssembler {

    public OperationResultTransferObject createOperationTransferObject(String resultString) {
        final OperationResultTransferObject transferObject = new OperationResultTransferObject();

        if (resultString.matches(".*\\(.*\\).*")) {
            final String operation = resultString.split("\\(")[0];
            final List<Double> operands = Arrays.stream(resultString
                    .split("\\(")[1]
                    .split("\\)")[0]
                    .split(", "))
                    .map(Double::valueOf)
                    .collect(Collectors.toList());
            final Double result = getResult(resultString);

            transferObject.setOperation(operation);
            transferObject.setOperands(operands);
            transferObject.setResult(result);
        } else {
            final String operation = resultString
                    .split(" = ")[0]
                    .split(" ")[1];
            final List<Double> operands = Arrays.stream(resultString
                    .split(" = ")[0]
                    .split(" . "))
                    .map(Double::valueOf)
                    .collect(Collectors.toList());
            final Double result = getResult(resultString);

            transferObject.setOperation(operation);
            transferObject.setOperands(operands);
            transferObject.setResult(result);
        }

        return transferObject;
    }

    private Double getResult(String resultString) {
        return Double.valueOf(resultString.split(" = ")[1]);
    }
}
