package calculator.rest;

import calculator.engine.FileHandler;
import calculator.engine.InvalidStatementException;
import calculator.engine.OperationHandler;
import calculator.rest.assembler.ResultAssembler;
import calculator.rest.transferobject.OperationResultTransferObject;
import calculator.rest.transferobject.OperationTransferObject;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

@Path("/")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class OperationResources {

    private OperationHandler operationHandler;
    private ResultAssembler resultAssembler;

    public OperationResources() throws NoSuchMethodException, IOException, InstantiationException, IllegalAccessException, InvocationTargetException, ClassNotFoundException {
        operationHandler = new OperationHandler(new FileHandler().configFileRead());
        resultAssembler = new ResultAssembler();
    }

    @POST
    @Path("singleOperation")
    public Response calculateSingleOperationResult(OperationTransferObject transferObject) {
        try {
            final String resultString = operationHandler.parseOperationRow(transferObject.createInputForOperationHandler());
            final OperationResultTransferObject resultTransferObject =
                    resultAssembler.createOperationTransferObject(resultString);

            return Response
                    .ok()
                    .entity(resultTransferObject)
                    .build();
        } catch (InvalidStatementException e) {
            return Response
                    .serverError()
                    .build();
        }
    }

    @POST
    @Path("batchOperations")
    public Response calculateBatchOperationsResults(List<OperationTransferObject> transferObjectList) {
        final List<OperationResultTransferObject> resultTransferObjectList = new ArrayList<>();

        for (OperationTransferObject transferObject: transferObjectList) {
            try {
                final String resultString = operationHandler.parseOperationRow(transferObject.createInputForOperationHandler());
                final OperationResultTransferObject resultTransferObject =
                        resultAssembler.createOperationTransferObject(resultString);

                resultTransferObjectList.add(resultTransferObject);
            } catch (InvalidStatementException e) {
                System.out.println("Wrong expression: " + transferObject.createInputForOperationHandler());
            }
        }

        return Response
                .ok()
                .entity(resultTransferObjectList)
                .build();
    }

}
