To run the REST application follow these steps:

1. First of all you need a wildfly12+ application server
2. Run the server ($WILDFLY_HOME$/bin/standalone.bat - on Windows)
3. Access the http://localhost:9990 address (admin platform of the WildFly)
4. Go to Deployments -> Add -> Upload a new deployment
    4.1 Choose the calculator-rest-1.0.war file
    4.2 Next -> Finish
5. Now you can test the application from Postman
    5.1 Import the JavaCalculator.postman_collection.json into Postman
    5.2 Test the application with these requests