package calculator.engine;

public abstract class OperationBase {
    private double result;

    /**
     * Set the result of equation.
     * @param result the equation result.
     */
    protected void setResult(double result) {
        this.result = result;
    }

    /**
     * Get the result of equation.
     * @return the equation result.
     */
    public double getResult() {
        return result;
    }

    /**
     * Calculate the operation result.
     * @throws InvalidStatementException if something is wrong with given statement.
     */
    public abstract void calculate() throws InvalidStatementException;
}
