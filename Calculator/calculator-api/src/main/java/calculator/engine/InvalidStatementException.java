package calculator.engine;

public class InvalidStatementException extends Exception {
    public InvalidStatementException(String reason, String statement, Throwable cause){
        super(reason + ": " + statement, cause);
    }

    public InvalidStatementException(String reason, String statement){
        super(reason + ": " + statement);
    }
}
