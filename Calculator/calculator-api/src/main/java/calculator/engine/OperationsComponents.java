package calculator.engine;

/**
 * This interface is implemented by every operation class.
 */
public interface OperationsComponents {
    /**
     * This method returns the associated keyword of operation.
     * @return the associated keyword to the operation
     */
    String getKeyword();

    /**
     * This method returns the associated symbol of operation.
     * @return the associated symbol to the operation.
     */
    char getSymbol();

    /**
     * Calculate the operation result.
     * @param value are the operands.
     * @return the result
     * @throws InvalidStatementException if something is wrong with given statement
     */
    double calculation(double... value) throws InvalidStatementException;
}
