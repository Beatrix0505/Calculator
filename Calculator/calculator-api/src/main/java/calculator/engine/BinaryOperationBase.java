package calculator.engine;

/**
 * This abstract class is extended by every operation class.
 */
public abstract class BinaryOperationBase extends OperationBase {
    private double lValue;
    private double rValue;

    protected BinaryOperationBase() {
    }

    protected BinaryOperationBase(double lValue, double rValue) {
        this.lValue = lValue;
        this.rValue = rValue;
    }

    /**
     * Set the left value.
     * @param lValue is a left value from equation.
     */
    private void setlValue(double lValue) {

        this.lValue = lValue;
    }

    /**
     * Get the left value.
     * @return the left value.
     */
    protected double getlValue() {

        return lValue;
    }

    /**
     * Set the right value.
     * @param rValue is a right value from equation.
     */
    public void setrValue(double rValue) {

        this.rValue = rValue;
    }

    /**
     * Get the right value.
     * @return the right value.
     */
    protected double getrValue() {

        return rValue;
    }

    public double calculation(double... value) throws InvalidStatementException{
        setlValue(value[0]);
        setrValue(value[1]);
        calculate();

        return getResult();
    }

}
