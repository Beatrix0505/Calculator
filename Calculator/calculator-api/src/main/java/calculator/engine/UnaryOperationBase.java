package calculator.engine;

public abstract class UnaryOperationBase extends OperationBase {
    private double value;

    protected UnaryOperationBase() {

    }

    protected UnaryOperationBase(double value) {
        this.value = value;
    }

    /**
     * Set the array of values.
     *
     * @param value is the array with equation values.
     */
    public void setValue(double value) {
        this.value = value;
    }

    /**
     * Get the array of values.
     *
     * @return the array with equation values.
     */
    protected double getValue() {
        return value;
    }

    public double calculation(double... value) throws InvalidStatementException {
        setValue(value[0]);
        calculate();

        return getResult();
    }
}
