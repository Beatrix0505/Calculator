package calculator.operations;

import calculator.engine.InvalidStatementException;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class DividerTest {

    private Divider divider;

    @Before
    public void setUp() {
        divider = new Divider(10.0, 5.0);
    }

    @Test
    public void testDividerCalculateInGoodCase() throws InvalidStatementException {
        divider.calculate();
        Double result = divider.getResult();

        assertEquals(new Double(2.0), result);
    }

    @Test(expected = InvalidStatementException.class)
    public void testDividerCalculateInBadCase() throws InvalidStatementException {
        divider.setrValue(0.0);
        divider.calculate();
    }

    @Test
    public void testDividerGetKeyword() {
        String keyword = divider.getKeyword();

        assertEquals("divide", keyword);
    }

    @Test
    public void testDividerGetSymbol() {
        Character symbol = divider.getSymbol();

        assertEquals(new Character('/'), symbol);
    }
}