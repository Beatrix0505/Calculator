package calculator.operations;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class PowerTest {

    private Power power;

    @Before
    public void setUp(){
        power = new Power(3.0, 3.0);
    }

    @Test
    public void testPowerCalculate() {
        power.calculate();
        Double result = power.getResult();

        assertEquals(new Double(27.0), result);
    }

    @Test
    public void testPowerGetKeyword() {
        String keyword = power.getKeyword();

        assertEquals("pow", keyword);
    }

    @Test
    public void testPowerGetSymbol() {
        Character symbol = power.getSymbol();

        assertEquals(new Character('^'), symbol);
    }
}