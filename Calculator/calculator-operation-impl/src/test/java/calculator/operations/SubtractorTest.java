package calculator.operations;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class SubtractorTest {

    private Subtractor subtractor;

    @Before
    public void setUp() {
        subtractor = new Subtractor(7.0, 1.0);
    }

    @Test
    public void testSubtractorCalculate() {
        subtractor.calculate();
        Double result = subtractor.getResult();

        assertEquals(new Double(6.0), result);
    }

    @Test
    public void testSubtractorGetKeyword() {
        String keyword = subtractor.getKeyword();

        assertEquals("subtract", keyword);
    }

    @Test
    public void testSubtractorGetSymbol() {
        Character symbol = subtractor.getSymbol();

        assertEquals(new Character('-'), symbol);
    }
}