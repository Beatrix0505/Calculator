package calculator.operations;

import calculator.engine.InvalidStatementException;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class InverterTest {

    private Inverter inverter;

    @Before
    public void setUp() {
        inverter = new Inverter(2.0);
    }

    @Test
    public void testInverterCalculateInGoodCase() throws InvalidStatementException {
        inverter.calculate();
        Double result = inverter.getResult();

        assertEquals(new Double(0.5), result);
    }

    @Test(expected = InvalidStatementException.class)
    public void testInverterCalculateInBadCase() throws InvalidStatementException {
        inverter.setValue(0.0);
        inverter.calculate();

    }

    @Test
    public void testInverterGetKeyword() {
        String keyword = inverter.getKeyword();

        assertEquals("inv", keyword);
    }

    @Test
    public void testInverterGetSymbol() {
        Character symbol = inverter.getSymbol();

        assertEquals(new Character('/'), symbol);
    }
}