package calculator.operations;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class AverageTest {

    private Average average;

    @Before
    public void setUp(){
        average = new Average(1.0, 2.0, 3.0);
    }

    @Test
    public void testAverageCalculate() {
        average.calculate();
        Double result = average.getResult();

        assertEquals(new Double(2.0), result);
    }

    @Test
    public void testAverageGetKeyword() {
        String keyword = average.getKeyword();

        assertEquals("avg", keyword);
    }

    @Test
    public void testAverageGetSymbol() {
        Character symbol = average.getSymbol();

        assertEquals(new Character('#'), symbol);
    }
}