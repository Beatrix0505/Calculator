package calculator.operations;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class AdderTest {

    private Adder adder;

    @Before
    public final void setUp() {
        adder = new Adder(3.0, 4.0);
    }

    @Test
    public void testAdderGetKeyword() {
        String keyword = adder.getKeyword();
        assertEquals("add", keyword);
    }

    @Test
    public void testAdderGetSymbol() {
        Character symbol = adder.getSymbol();
        assertEquals(new Character('+'), symbol);
    }

    @Test
    public void testAdderCalculate() {
        adder.calculate();
        Double result = adder.getResult();

        assertEquals(new Double(7.0), result);
    }
}