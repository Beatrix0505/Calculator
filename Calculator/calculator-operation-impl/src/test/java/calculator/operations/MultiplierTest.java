package calculator.operations;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MultiplierTest {

    private Multiplier multiplier;

    @Before
    public void setUp(){
        multiplier = new Multiplier(2.0,3.0);
    }

    @Test
    public void testMultiplierCalculate() {
        multiplier.calculate();
        Double result = multiplier.getResult();

        assertEquals(new Double(6.0), result);
    }

    @Test
    public void testMultiplierGetKeyword() {
        String keyword = multiplier.getKeyword();

        assertEquals("multiply", keyword);
    }

    @Test
    public void testMultiplierGetSymbol() {
        Character symbol = multiplier.getSymbol();

        assertEquals(new Character('*'), symbol);
    }
}