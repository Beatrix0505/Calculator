package calculator.operations;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class SumTest {

    private Sum sum;

    @Before
    public void setUp() {
        sum = new Sum(11.0, 12.0, 7.0);
    }

    @Test
    public void testSumGetKeyword() {
        String keyword = sum.getKeyword();

        assertEquals("sum", keyword);
    }

    @Test
    public void testSumGetSymbol() {
        Character symbol = sum.getSymbol();

        assertEquals(new Character('+'), symbol);
    }

    @Test
    public void testSumCalculate() {
        sum.calculate();
        Double result = sum.getResult();

        assertEquals(new Double(30.0), result);
    }
}