package calculator.operations;

import calculator.engine.AggregationBase;
import calculator.engine.OperationsComponents;

public class Average extends AggregationBase implements OperationsComponents {

    Average(double... value) {
        super(value);
    }

    public Average() {
    }

    @Override
    public void calculate() {
        double result = 0;
        for (double aValue : getValue()) {
            result += aValue;
        }
        int size = getValue().length;
        result /= size;
        setResult(result);
    }

    @Override
    public String getKeyword() {
        return "avg";
    }

    @Override
    public char getSymbol() {
        return '#';   // saját kitaláció
     }
}
