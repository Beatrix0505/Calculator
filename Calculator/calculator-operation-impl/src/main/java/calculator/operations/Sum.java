package calculator.operations;

import calculator.engine.AggregationBase;
import calculator.engine.OperationsComponents;

public class Sum extends AggregationBase implements OperationsComponents {
    Sum(double... value) {
        super(value);
    }

    public Sum() {
    }

    @Override
    public String getKeyword() {
        return "sum";
    }

    @Override
    public char getSymbol() {
        return '+';
    }

    @Override
    public void calculate() {
        double result = 0;
        for (double aValue : getValue()) {
            result += aValue;
        }
        setResult(result);
    }
}
