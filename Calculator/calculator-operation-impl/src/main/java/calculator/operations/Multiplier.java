package calculator.operations;

import calculator.engine.BinaryOperationBase;
import calculator.engine.OperationsComponents;

public class Multiplier extends BinaryOperationBase implements OperationsComponents {

    Multiplier(double lValue, double rValue) {
        super(lValue, rValue);
    }

    public Multiplier() {
    }

    @Override
    public void calculate() {
        double result = getlValue() * getrValue();
        setResult(result);
    }

    @Override
    public String getKeyword() {
        return "multiply";
    }

    @Override
    public char getSymbol() {
        return '*';
    }

}
