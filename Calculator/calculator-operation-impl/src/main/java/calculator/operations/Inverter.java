package calculator.operations;

import calculator.engine.InvalidStatementException;
import calculator.engine.OperationsComponents;
import calculator.engine.UnaryOperationBase;

public class Inverter extends UnaryOperationBase implements OperationsComponents {

    Inverter(double value) {
        super(value);
    }

    public Inverter() {
    }

    @Override
    public void calculate() throws InvalidStatementException {
        double value = getValue();
        if (value == 0.0){
            throw new InvalidStatementException("Division by zero ", "inv " + String.valueOf(value));
        }
        double result = 1 / value;
        setResult(result);
    }

    @Override
    public String getKeyword() {
        return "inv";
    }

    @Override
    public char getSymbol() {
        return '/';
    }
}
