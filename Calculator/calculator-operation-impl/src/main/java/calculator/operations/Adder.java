package calculator.operations;

import calculator.engine.BinaryOperationBase;
import calculator.engine.OperationsComponents;

public class Adder extends BinaryOperationBase implements OperationsComponents {
    Adder(double lValue, double rValue) {
        super(lValue, rValue);
    }

    public Adder() {
    }

    @Override
    public String getKeyword() {
        return "add";
    }

    @Override
    public char getSymbol() {
        return '+';
    }

    @Override
    public void calculate() {
        double result = getlValue() + getrValue();
        setResult(result);
    }
}
