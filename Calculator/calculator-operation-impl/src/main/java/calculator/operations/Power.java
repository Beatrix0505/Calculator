package calculator.operations;

import calculator.engine.BinaryOperationBase;
import calculator.engine.OperationsComponents;

public class Power extends BinaryOperationBase implements OperationsComponents {
    Power(double lValue, double rValue) {
        super(lValue, rValue);
    }

    public Power() {
    }

    @Override
    public void calculate() {
        double result = Math.pow(getlValue(),getrValue());
        setResult(result);
    }

    @Override
    public String getKeyword() {
        return "pow";
    }

    @Override
    public char getSymbol() {
        return '^';
    }

}
