package calculator.operations;

import calculator.engine.BinaryOperationBase;
import calculator.engine.OperationsComponents;

public class Subtractor extends BinaryOperationBase implements OperationsComponents {
    Subtractor(double lValue, double rValue) {
        super(lValue, rValue);
    }

    public Subtractor() {
    }

    @Override
    public void calculate() {
        double result = getlValue() - getrValue();
        setResult(result);
    }

    @Override
    public String getKeyword() {
        return "subtract";
    }

    @Override
    public char getSymbol() {
        return '-';
    }

}
