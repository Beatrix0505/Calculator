package calculator.operations;

import calculator.engine.BinaryOperationBase;
import calculator.engine.InvalidStatementException;
import calculator.engine.OperationsComponents;

public class Divider extends BinaryOperationBase implements OperationsComponents {

    Divider(double lValue, double rValue) {
        super(lValue, rValue);
    }

    public Divider() {
    }

    @Override
    public void calculate() throws InvalidStatementException {
        double lValue = getlValue();
        double rValue = getrValue();
        if (rValue == 0){
            throw new InvalidStatementException("Division by zero ", "divide " + String.valueOf(lValue) + " " + String.valueOf(rValue));
        }
        double result = lValue / rValue;
        setResult(result);
    }

    @Override
    public String getKeyword() {
        return "divide";
    }

    @Override
    public char getSymbol() {
        return '/';
    }

}
